# Safe Haven Service Superdome Flex Training

This workshop introduces users of the Safe Haven Service (SHS) to the Superdome Flex (SDF) computing cluster. 
The SDF is a shared resource across SHS users unlike the Desktop VMs, which are built for exclusive use by a user. 
It is important that all users are aware of the standard workflow for using the SDF so demand can be fairly managed.

These materials are intended to be detailed enough for users to self-teach. However, courses taught by instructors may be arranged if there is sufficient demand. Please suggest any improvements as issue tickets on the online GitLab repository https://git.ecdf.ed.ac.uk/hsamuel/shs_sdf_training/-/issues.

`Note: The file system inside the SDF is separate from the file systems inside the Deskop VMs and is not intended to be used for permanent storage.`
`Data stored on the SDF is protected by Linux permissions, but it is not held in entirely separate spaces as in the Desktop VMs.`
`Users should only transfer the minimal amount of cohort data required for an analysis to be completed and delete it as soon as possible.`

## Workshop Plan

|Lesson|Objective|
|--|--|
|<a href="https://git.ecdf.ed.ac.uk/hsamuel/shs_sdf_training/-/blob/main/lessons/L1_accessing_the_SDF_inside_the_SHS.md">Accessing the SDF inside the SHS</a>|a. What is the Superdome Flex?<br>b. How do I request access to the SDF?<br>c. How do I run scripts on the SDF?<br>d. Is the SDF file system linked to my VM?|
|<a href="https://git.ecdf.ed.ac.uk/hsamuel/shs_sdf_training/-/blob/main/lessons/L2_running_simple_R_Python_analysis_scripts.md">Running simple R/Python analysis scripts</a>|a. Using conda|
|<a href="https://git.ecdf.ed.ac.uk/hsamuel/shs_sdf_training/-/blob/main/lessons/L3_submitting_scripts_to_slurm.md">Submitting scripts to Slurm</a>|a. What is Slurm?<br>b. Why do I need to use Slurm?<br>|
|<a href="https://git.ecdf.ed.ac.uk/hsamuel/shs_sdf_training/-/blob/main/lessons/L4_parallelised_python_analysis.md">Parallelised Python Analysis</a>|a. What is Dask?<br>b. How do I start to parallelise my python code?<br>c. How do a parallelise more complex tasks?|
|<a href="https://git.ecdf.ed.ac.uk/hsamuel/shs_sdf_training/-/blob/main/lessons/L5_parallelised_r_analysis.md">Parallelised R Analysis</a>|a. How do I parallelise my R code?<br>b. What R packages enable parallelised execution?|

## Extended Workshop Plan
|Lesson|Objective|
|--|--|
|Using workflow managers||
|Containerising analysis pipelines||
|Basic profiling||

## Contributions

These notes were originally adapted from Adrian Jackson's Archer2 for data scientists course https://github.com/EPCCed/archer2-data-science.  

